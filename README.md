<p align="center"><img src="https://codescompanion.com/wp-content/uploads/2020/03/Laravel-image-optimisation.png" width="400"></p>

## Step by Step Laravel with Spatie ACL Role Permission

## Install Your Laravel Project
>  composer create-project --prefer-dist laravel/laravel project "version"

```
composer create-project --prefer-dist laravel/laravel blog "5.8.*"

```

## Install Composer Packages

```
composer require spatie/laravel-permission

composer require 'laravelcollective/html:^5.8.0'

```

## Register Provider and Aliases

Now open config/app.php file and add service provider and aliase.
>  config/app.php

```
'providers' => [

	....

	Spatie\Permission\PermissionServiceProvider::class,

	Collective\Html\HtmlServiceProvider::class,

],

'aliases' => [

	....

	'Form' => Collective\Html\FormFacade::class,

	'Html' => Collective\Html\HtmlFacade::class,

],

```


We can also custom changes on Spatie package, so if you also want to changes then you can fire bellow command and get config file in config/permission.php.

```
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="config"

```

## Create Migrations

In this step we have to create three migrations for as listed bellow tables:

1. users
2. products
3. roles
4. permissions
5. model_has_permissions
6. model_has_roles
7. role_has_permissions

So, if you install fresh project then you have already users table migration but if you don't have products table, so can create manually and other table can create using Spatie package command, so run bellow command and check migration file also.

```
php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider" --tag="migrations"

php artisan make:migration create_products_table
```


> Users Table

```
<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

```

> products table

```

<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('detail');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

```


> Spatie tables

```

<?php


use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreatePermissionTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $tableNames = config('permission.table_names');


        Schema::create($tableNames['permissions'], function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });


        Schema::create($tableNames['roles'], function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('guard_name');
            $table->timestamps();
        });


        Schema::create($tableNames['model_has_permissions'], function (Blueprint $table) use ($tableNames) {
            $table->integer('permission_id')->unsigned();
            $table->morphs('model');


            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');


            $table->primary(['permission_id', 'model_id', 'model_type']);
        });


        Schema::create($tableNames['model_has_roles'], function (Blueprint $table) use ($tableNames) {
            $table->integer('role_id')->unsigned();
            $table->morphs('model');


            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');


            $table->primary(['role_id', 'model_id', 'model_type']);
        });


        Schema::create($tableNames['role_has_permissions'], function (Blueprint $table) use ($tableNames) {
            $table->integer('permission_id')->unsigned();
            $table->integer('role_id')->unsigned();


            $table->foreign('permission_id')
                ->references('id')
                ->on($tableNames['permissions'])
                ->onDelete('cascade');


            $table->foreign('role_id')
                ->references('id')
                ->on($tableNames['roles'])
                ->onDelete('cascade');


            $table->primary(['permission_id', 'role_id']);


            app('cache')->forget('spatie.permission.cache');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $tableNames = config('permission.table_names');


        Schema::drop($tableNames['role_has_permissions']);
        Schema::drop($tableNames['model_has_roles']);
        Schema::drop($tableNames['model_has_permissions']);
        Schema::drop($tableNames['roles']);
        Schema::drop($tableNames['permissions']);
    }
}

```

